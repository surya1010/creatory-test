# Creatory Test

creatory test

- git clone 
- touch .env when not found in this repository
- docker-compose up --build

### example .env
```HTTP_PORT=8080 
DB_USER=root
DB_PASSWORD=password
DB_NAME=creatory
DB_HOST=mariadb
DB_PORT=3306
```

## Notes
maybe when first run `docker-compose up -build` takes time create `database`. since with mysql database, name database must first initialize

#### endpoint
1. Create Currency
```
curl --location --request POST 'http://localhost:8080/currency' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name" : "knut"
}'
```
2. Get Currency `curl --location --request GET 'http://localhost:8080/currency'`
3. Create Conversion rate
```
curl --location --request POST 'http://localhost:8080/conversion-rate' \
--header 'Content-Type: application/json' \
--data-raw '{
    "currencyIDFrom" : 2,
    "currencyIDTo" : 1,
    "rate": 29

}'
```
4. Create Rate Converted
```
curl --location --request POST 'http://localhost:8080/rate-converted' \
--header 'Content-Type: application/json' \
--data-raw '{
    "currencyIDFrom" : 1,
    "currencyIDTo" : 2,
    "amount": 580

}'
```



